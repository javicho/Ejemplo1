USE [Ejemplo1]
GO
/****** Object:  Table [dbo].[clientes]    Script Date: 28/08/2018 03:49:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[clientes](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Razon_social] [varchar](100) NULL,
	[Nombre_comercial] [varchar](100) NULL,
	[RFC] [varchar](50) NULL,
	[CURP] [varchar](50) NULL,
	[Direccion] [varchar](50) NULL,
 CONSTRAINT [PK_clientes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[clientes] ON 

INSERT [dbo].[clientes] ([id], [Razon_social], [Nombre_comercial], [RFC], [CURP], [Direccion]) VALUES (1, N'prueba', N'prueba n', N'AAA010101AAA', N'BADD110313HCMLNS09

', N'prueba d')
INSERT [dbo].[clientes] ([id], [Razon_social], [Nombre_comercial], [RFC], [CURP], [Direccion]) VALUES (4, N'ibocspS', N'haoido', N'onoda', N'onjod', N'onos dsol')
INSERT [dbo].[clientes] ([id], [Razon_social], [Nombre_comercial], [RFC], [CURP], [Direccion]) VALUES (5, N'qeoqe', N'odfnip', N'npadp', N'knpwnpnw', N'kwbjjor iorwo')
SET IDENTITY_INSERT [dbo].[clientes] OFF
